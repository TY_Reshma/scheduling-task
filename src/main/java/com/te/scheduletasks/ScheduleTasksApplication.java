package com.te.scheduletasks;

import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
public class ScheduleTasksApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScheduleTasksApplication.class, args);
	}

	/**
	 * This method specifies that it's execute at fixed time period
	 * It doesn't wait for previous task to complete.
	 * @throws InterruptedException
	 */
	
	@SuppressWarnings("deprecation")
	@Scheduled(fixedRate  = 1000)
	public void scheduledTaskFixedRate() throws InterruptedException {
		System.err.println("***************Fixed rate*****************"+new Date().getSeconds());
		Thread.sleep(2000);
	}
	
	/**
	 * This method specifies that it execute at fixed time period between the end of the previous invocation and 
	 * the start of the next invocations
	 * @throws InterruptedException
	 */
	@SuppressWarnings("deprecation")
	@Scheduled(fixedRate  = 1000)
	public void scheduledTaskFixedDelay() throws InterruptedException {
		System.err.println("***************Fixed Delay*****************"+new Date().getSeconds());
		Thread.sleep(2000);
	}
	
	/**
	 * crone is used to generate crone expression 
	 * this method is executed when that time interval will come.
	 * @throws InterruptedException
	 */
	@SuppressWarnings("deprecation")
	@Scheduled(cron = "0 0 12 1/1 *  *")
	public void scheduledTaskCrone() throws InterruptedException {
		System.err.println("***************crone*****************"+new Date().getSeconds());
		Thread.sleep(2000);
	}
	
	
	
	
	
	
	
}
